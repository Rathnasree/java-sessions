package day4.concurrency;

import java.util.List;
import java.util.ArrayList;

public class Customer {
    public static void main(String[] args) throws InterruptedException {
        Printer printer = new Printer();
        
        Task task1 = new Task(printer, "John", 4);
        Task task2 = new Task(printer, "Harish", 3);
        Task task3 = new Task(printer, "Seema", 6);
        Task task4 = new Task(printer, "Vikram", 5);

        List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);

        List<Thread> threads = new ArrayList<>();
        tasks.forEach(task -> {
            Thread t = new Thread(task);
            threads.add(t);
            t.start();
        });

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        System.out.println(" Waiting for all the threads to complete");
    }
}

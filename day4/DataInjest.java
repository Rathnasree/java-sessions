package day4;

import java.util.*;

import static java.nio.file.Files.newBufferedReader;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toSet;
import java.io.*;

public class DataInjest {

    private static final String fileLocation = "D:\\data\\address.csv";
    
    public static Set<User> populateUsers(){
        Set<User> users = new HashSet<>();
        try (
            BufferedReader reader =  newBufferedReader(get(fileLocation));
        ) {
                users = reader.lines()
                            .skip(1)
                            .map(DataInjest::mapLineToUser)
                            .collect(toSet());

          } catch(Exception e) {
                e.printStackTrace();
            }
            return users;
    }

    private static User mapLineToUser(String line) {
        String [] data = line.split(",");
        String firstName = data[0];
        String lastName = data[1];
        String company = data[2];
        String city = data[4];
        String county = data[5];
        String state = data[6];
        int zipCode = 0;
        try {
         zipCode = Integer.parseInt(data[7]);
        } catch(Exception e){
            System.out.println(line);
        }
        User user = new User(firstName, lastName, company, city, state, county, zipCode);
        return user;
    }

    public static void main(String[] args) {
        populateUsers();
    }
}


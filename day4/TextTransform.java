package day4;

import java.util.stream.Stream;

import static java.lang.System.out;

import java.util.*;

public class TextTransform {

    public static void main(String[] args) {
        Stream
            .of("one", "one", "two","two", "three", "two", "five","five", "seventeen", "seventeen")
            .distinct()
            .map(String :: toUpperCase)
            .sorted(String::compareTo)
            .skip(1)
            .forEach(out::println);
    }
}

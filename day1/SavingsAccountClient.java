package day1;

public class SavingsAccountClient {
    public static void main(String args[]){
        //ReferenceType handle = new ReferenceType();
        SavingsAccount account1 = new SavingsAccount();
        SavingsAccount account2 = new SavingsAccount();

        double initialAccountBalance1 = account1.checkBalance();
        double initialAccountBalance2 = account2.checkBalance();
        System.out.printf("Initial account balance of account1 is :: %f %n ", initialAccountBalance1);
        System.out.printf("Initial account balance of account2 is :: %f %n ", initialAccountBalance2);

        account1.deposit(35000);
        account2.deposit(50000);

        double accountBalanceAfterDeposit1 = account1.checkBalance();
        double accountBalanceAfterDeposit2 = account2.checkBalance();

        System.out.printf("Account balance of account 1 after deposit is :: %f %n ", accountBalanceAfterDeposit1);
        System.out.printf("Account balance of account 2 after deposit is :: %f %n ", accountBalanceAfterDeposit2);

        account1.withdraw(20000);
        account2.withdraw(25000);

        double accountBalanceAfterWithdraw1 = account1.checkBalance();
        double accountBalanceAfterWithdraw2 = account2.checkBalance();
        System.out.printf("Account balance of account 1 after withdrawal is :: %f %n ", accountBalanceAfterWithdraw1);
        System.out.printf("Account balance of account 2 after withdrawal is :: %f %n ", accountBalanceAfterWithdraw2);
    }
}

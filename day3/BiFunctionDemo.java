package day3;

import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.function.Consumer;

public class BiFunctionDemo {

    public static void main(String[] args) {
        BiFunction<Integer, Integer, Integer> sum = (val1, val2) -> val1 + val2;
        BiFunction<Integer, Integer, Integer> product = (val1, val2) -> val1 * val2;
        BiFunction<Integer, Integer, Integer> difference = (val1, val2) -> val1 - val2;

        Supplier<Address> generateAddress = () -> new Address("Bangalore", "state", 51147);

        System.out.println("The sum of numbers is "+ sum.apply(34, 55));;

        int response = compute(product, 45, 56);

        System.out.println("Response :: "+ response);

        Consumer<Address> addressConsumer = (address)-> System.out.println(address.getCity());

        addressConsumer.accept(new Address("Bangalore", "state", 51147));

    }

    public static Integer compute (BiFunction<Integer, Integer, Integer> function, int a, int b){
       return function.apply(a, b); 
    }
    
}

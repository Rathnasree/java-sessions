package day3;

import java.util.Comparator;

public class Comparators {
    public static Comparator<Address> addressZipCodeAscComparator = (address1, address2) -> address1.getZipCode() - address2.getZipCode();
    public static Comparator<Address> addressZipCodeDescComparator = addressZipCodeAscComparator.reversed();
}

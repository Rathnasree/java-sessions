package day3;

import java.util.HashSet;
import java.util.OptionalInt;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class StreamsDemo {
    public static void main(String[] args) {
        Set<User> users = DataInjest.populateUsers();
        Product iPhone = new Product("IPhone-11", 75_000, Brand.APPLE);
        Product iPad = new Product("IPad", 35_000, Brand.APPLE);
        Product galaxyS11 = new Product("Samgung Galaxy-S11", 75_000, Brand.SAMSUNG);
        Product thinkpad = new Product("ThinkPad-T580", 1_75_000, Brand.LENOVO);
        Product thinkpad2 = new Product("ThinkPad-T580", 2_75_000, Brand.LENOVO);
        Product lg = new Product("LG", 25000, Brand.LG);

        Set<Product> products = new HashSet<>();
        products.add(iPhone);
        products.add(iPad);
        products.add(galaxyS11);
        products.add(thinkpad);
        products.add(thinkpad2);
        products.add(lg);

        Predicate<Product> isIPhone =   (product) -> product.getName().equalsIgnoreCase("IPhone-11");
        Predicate<Product> isIPad =   (product) -> product.getName().equalsIgnoreCase("IPad");
        Predicate<Product> isSamsung =   (product) -> product.getBrand() == Brand.SAMSUNG;
        Predicate<Product> isLS =   (product) -> product.getBrand() == Brand.LG;
        Predicate<Product> isLennovo =   (product) -> product.getBrand() == Brand.LENOVO;

        Predicate<Product> isApple = isIPhone.or(isIPad);
        Predicate<Product> isNotApple = isIPhone.or(isIPad).negate();
        // System.out.println("Is from Apple ?:: "+ isApple.test(iPad));
        // System.out.println("Is from Apple ?:: "+ isApple.test(iPhone));

        Predicate<Product> isNotAppleAndGtThan1L = isNotApple.and(product -> product.getPrice() > 1_00_000);

        long numberOfAppleProducts = products.stream()
                    .filter(isApple)
                    .count();
        System.out.println("Number of apple products "+ numberOfAppleProducts);

        Predicate<User> userByState = user -> user.getState().equals("LA");
        products
            .stream()
            .map((product) -> product.getName())
            .map(name -> name.toUpperCase())
            .distinct()
            .forEach(product -> System.out.println(product));
            
            System.out.println("Number of users in LA state :: " +users.stream()
                .filter(userByState)
                .count());

/*             users.stream()
                        .filter(userByState)
                        .distinct()
                        .map(user -> user.getFirstName() + " "+ user.getLastName() + " state:: "+ user.getState())
                        .forEach(name -> System.out.println(name));
 */
           
                users.stream()
                .filter(userByState)
                .distinct()
                .map(user -> user.getFirstName() + " "+ user.getLastName() + " state:: "+ user.getState())
                .limit(3)
                .forEach(name -> System.out.println(name));

                Stream<Integer> data = Stream.of(1,2,3,4, 5,6);

                IntStream dataStream = IntStream.range(10, 45);
                OptionalInt max = dataStream
                    //.map(val -> val * 23)
                    //.filter(num -> num % 3 == 0)
                    .max();
                
            max.ifPresent(value -> System.out.println("Max value is "+ value));
    }
}



class Product {
    private String name;
    private double price;
    private Brand brand;

    public Product(String name, double price, Brand brand){
        this.name = name;
        this.price = price;
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Product [brand=" + brand + ", name=" + name + ", price=" + price + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((brand == null) ? 0 : brand.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        long temp;
        temp = Double.doubleToLongBits(price);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (brand != other.brand)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
            return false;
        return true;
    }
   
    
}

enum Brand {
    APPLE,
    SAMSUNG,
    LG,
    LENOVO
}

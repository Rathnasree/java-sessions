package day3;

public interface PhoneRecharge {

     void recharge(String phoneNumber, double amount);
    
}
